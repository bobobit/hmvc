<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_equipment extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get(){

    	$query = $this->db->get('equipment');

    	return $query->result();

    }
}