<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Equipment extends MX_Controller {

	function __construct() {
        parent::__construct();

        $this->data = array();

		$this->load->model('mdl_equipment');
		$this->data['equipment'] = $this->mdl_equipment->get();
    }

	public function index()	{

		$this->load->view('equipment', $this->data);
	}

	public function equipment_list()	{

		$this->load->view('equipment_list', $this->data);

	}

}