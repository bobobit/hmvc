<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module_with_widget extends MX_Controller {

	public function index()
	{
		$this->load->view('module_with_widget');
	}
}